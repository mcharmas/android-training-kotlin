package pl.charmas.training.kotlintraining

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    //TODO: ReadWriteProperty to shared preferences
    var storedContent: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        startButton.setOnClickListener {
            Intent(this, HelloActivity::class.java)
                    .putExtra("ARG_HELLO_TEXT", "Hello from Kotlin!")
                    .let { startActivity(it) }
        }

        storedContentView.setText(storedContent)
        storedContentView.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) = Unit
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = Unit
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                storedContent = p0.toString()
            }
        })
    }
}
