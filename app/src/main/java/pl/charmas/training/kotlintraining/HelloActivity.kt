package pl.charmas.training.kotlintraining

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_hello.*

class HelloActivity : AppCompatActivity() {
    //TODO get content from intent ARG_HELLO_TEXT
    private val textFromIntent = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)
        helloView.text = textFromIntent
    }
}
