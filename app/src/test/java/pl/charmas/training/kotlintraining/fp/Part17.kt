package pl.charmas.training.kotlintraining.fp

import org.junit.Assert.assertEquals
import org.junit.Test
import java.math.BigDecimal

data class OrderedProduct(val name: String, val price: BigDecimal)
data class Order(val orderedProduct: List<OrderedProduct>)

fun getNames(products: List<OrderedProduct>): List<String> = TODO()

fun getUniqueOrderedProducts(order: List<Order>): Set<OrderedProduct> = TODO()

fun sumAllOrderValue(order: List<Order>): BigDecimal = TODO()

fun countAllOrdersWithValueGreaterThan10(order: List<Order>): Int = TODO()

/**
 * Zadanie 1.
 * Zamień kolekcję produktów na kolekcję ich nazw.
 *
 * Zadanie 2.
 * Z listy zamówień wyciągnij unikalne wszystkie zakupione produkty.
 *
 * Zadanie 3.
 * Podsumuj wartość wszystkich zamówień.
 *
 * Zadanie 4.
 * Policz liczbę wszystkich zamówień o wartości powyżej 10 PLN.
 */
class Part17 {
    @Test
    fun zadanie1() {
        val products = listOf(
                OrderedProduct("a", BigDecimal(10)),
                OrderedProduct("b", BigDecimal(11)),
                OrderedProduct("c", BigDecimal(12))
        )

        assertEquals(listOf("a", "b", "c"), getNames(products))
    }

    @Test
    fun zadanie2() {
        assertEquals(5, getUniqueOrderedProducts(sampleOrders()).size)
    }

    @Test
    fun zadanie3() {
        assertEquals(82, sumAllOrderValue(sampleOrders()))
    }

    @Test
    fun zadanie4() {
        assertEquals(2, countAllOrdersWithValueGreaterThan10(sampleOrders()))
    }

    private fun sampleOrders(): List<Order> = listOf(
            Order(listOf(
                    OrderedProduct("a", BigDecimal(10)),
                    OrderedProduct("b", BigDecimal(11)),
                    OrderedProduct("c", BigDecimal(12))
            )),
            Order(listOf(
                    OrderedProduct("a", BigDecimal(10)),
                    OrderedProduct("b", BigDecimal(11)),
                    OrderedProduct("c", BigDecimal(12)),
                    OrderedProduct("d", BigDecimal(14))
            )),
            Order(listOf(OrderedProduct("e", BigDecimal(2))))
    )
}
