package pl.charmas.training.kotlintraining.basicsyntax

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test

fun parseInt(value: String): Int? = TODO()

/**
 * Zadanie 1.
 * Zaimplementuj funkcję parseInt tak aby zamieniała string na integer.
 * Użyj do tego funkcji Integer.parseInt(). Gdy parsowanie się nie powiedzie
 * funkcja powinna zwrócić null.
 *
 * Zadanie 2.
 * Zrefaktoruj funkcję tak, aby zwracała wartość wyrażenia try.
 *
 * Zadanie 3.
 * Zrefaktoruj ciało funkcji do wyrażenia.
 */
class Part3 {

    @Test
    fun parsesCorrectNumber() {
        assertEquals(5, parseInt("5"))
    }

    @Test
    fun name() {
    }

    @Test
    fun returnsNullWhenNotANumber() {
        assertNull(parseInt("test"))
    }
}
