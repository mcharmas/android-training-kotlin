package pl.charmas.training.kotlintraining.fp

import org.junit.Test
import java.math.BigDecimal

data class Product(val name: String, val price: BigDecimal)
data class ProductsCart(private val products: List<Product>)

/**
 * Zadanie 1.
 * Dodaj metodę do ProductsCart, która umożliwi sprawdzenie, czy produkt
 * spełniający dane kryteria jest w koszyku. Metoda ta powinna przyjmować funkcję
 * predykat, która na podstawie Productu będzie wzwracała boolean.
 *
 * Wykorzystaj tą funkcję aby sprawdzić czy w koszyku znajduje się produkt
 * "Proszek do prania" za cenę większą od 15 PLN.
 */
class Part15 {

    @Test
    fun higherOrderFunctionsPredicate() {
        val cart = ProductsCart(listOf(
                Product("Mydło", BigDecimal(12)),
                Product("Proszek do prania", BigDecimal(10)),
                Product("Proszek do prania", BigDecimal(16))
        ))
        // TODO
    }
}
