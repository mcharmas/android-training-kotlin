package pl.charmas.training.kotlintraining.oop

import org.junit.Assert.assertTrue
import org.junit.Test

class Employee(val name: String, val age: Int)

/**
 * Zadanie 1
 * Zaimplementuj equals i hashCode tak aby pracownik mógł zostać odnaleziony w organizacji.
 *
 * Zadanie 2
 * Skorzystaj z data class aby nie pisać ręcznie equals i hashCode
 *
 * Zadanie 3
 * Za pomocą destructing declaration wyciągnij i wypisz na ekran imię i wiek pracownika
 */
class Part10 {

    @Test
    fun organizationContainsEmployee() {
        val organisation = setOf(
                Employee("Adam", 20),
                Employee("Marta", 21)
        )
        assertTrue(organisation.contains(Employee("Adam", 20)))
    }
}
