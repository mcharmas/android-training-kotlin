package pl.charmas.training.kotlintraining.basicsyntax

import org.junit.Assert.assertEquals
import org.junit.Test

interface Expression
class Number(val value: Int) : Expression
class Sum(val left: Expression, val right: Expression) : Expression

fun evaluate(expression: Expression): Int = TODO()

/**
 * Zadanie 1.
 * Zaimplementuj funkcję, która dokona ewaluacji wyrażenia Expression używając ifów.
 *
 * Zadanie 2.
 * Zrefaktoruj funkcję tak, aby zwracała wartość wyrażenia if
 *
 * Zadanie 3.
 * Użyj konstrukcji składniowej when do implementacji tej funkcji
 *
 * Zadanie 4.
 * Zaloguj wartości (println) podczas kalkulacji funkcji eval.
 *
 */
class Part2 {

    @Test
    fun evaluatesWithSum() {
        assertEquals(12, evaluate(Sum(Number(5), Number(7))))
    }
}
