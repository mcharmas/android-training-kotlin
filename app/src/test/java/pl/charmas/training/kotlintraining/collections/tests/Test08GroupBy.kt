package pl.charmas.training.kotlintraining.collections.tests

import junit.framework.Assert.assertEquals
import org.junit.Test
import pl.charmas.training.kotlintraining.collections.exercises.groupCustomersByCity

class Test08GroupBy {
    @Test
    fun testGroupCustomersByCity() {
        assertEquals(groupedByCities, shop.groupCustomersByCity())
    }
}
