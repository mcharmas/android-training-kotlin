package pl.charmas.training.kotlintraining.oop

/**
 * Zadanie 1.
 * Utwórz nadpisaną implementację ViewAnimatora dodając do niego funkcjonalność
 * wyświetlenia dziecka po ID a nie po indexie.
 *
 * Zadanie 2.
 * Zwróć uwagę na konstruktury. Widok powinno dać się utworzyć zarówno przez inflatowanie
 * (wymagany konstruktor 2 parametrowy) jak i z kodu w javie (konstruktor 1 parametrowy)
 *
 * Zadanie 3.
 * Spełnij wymaganie z punktu pierwsze używając Extension Function zamiast dziedziczenia.
 */
class Part12 {
}
