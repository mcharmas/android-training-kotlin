package pl.charmas.training.kotlintraining.rx

import io.reactivex.rxkotlin.Observables
import io.reactivex.rxkotlin.toObservable
import org.junit.Test

class Part23 {

    @Test
    fun example1() {
        listOf(1, 2, 3).toObservable().subscribe { println(it) }
    }

    @Test
    fun example2() {
        val firstSource = listOf(1, 2, 3).toObservable()
        val secondSource = listOf(1, 2, 3).toObservable()
        Observables
                .combineLatest(firstSource, secondSource) { first, second -> first * second }
                .subscribe { println(it) }
    }
}
