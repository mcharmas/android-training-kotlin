package pl.charmas.training.kotlintraining.oop

import java.math.BigDecimal

data class Cart(val products: ProductInCart)
data class ProductInCart(val name: String, val pricing: BigDecimal)

interface CartPricingModifier {
    fun applyPricingChange(cart: Cart): Cart
}

class ShippingCostPricingModifier : CartPricingModifier {
    override fun applyPricingChange(cart: Cart): Cart = TODO()
}

/**
 * Zadanie 1.
 * Zaimplementuj modyfikator ceny, który dolicza 10 PLN do każdego produktu
 * jako cena dostawy. Wszystkie dane są niemodyfikowalne. Utwórz zaktualizowane
 * kopie obiektów używając metody copy() na obiektach data class.
 */
class Part11 {
}
