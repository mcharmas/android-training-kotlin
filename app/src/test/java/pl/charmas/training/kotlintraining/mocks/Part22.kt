package pl.charmas.training.kotlintraining.mocks

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Test
import java.util.*

class Sample {
    fun example(): Int = 1
}

interface TimeProvider {
    fun getCurrentTime(): Date
}

data class Order(val creationTime: Date)

class OrderFactory(private val timeProvider: TimeProvider) {
    fun createNewOrder() = Order(timeProvider.getCurrentTime())
}

/**
 * Zadanie 1.
 * Przetestuj w izolacji fabrykę zamówień.
 */
class Part22 {

    @Test
    fun mocksFinalClass() {
        val mockedSample = mock<Sample> {
            on { example() } doReturn 2
        }

        assertEquals(2, mockedSample.example())
    }

    @Test
    fun createdOrderShouldHaveCurrentDate() {
        TODO()
    }
}
