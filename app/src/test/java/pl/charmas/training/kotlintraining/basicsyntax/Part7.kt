package pl.charmas.training.kotlintraining.basicsyntax

import org.junit.Assert.assertTrue
import org.junit.Test
import java.util.*

class LocalDate(val year: Int, val month: Int, val dayOfMonth: Int) : Comparable<LocalDate> {
    override fun compareTo(other: LocalDate): Int = TODO()

    operator fun rangeTo(other: LocalDate): DateRange = DateRange(this, other)

    fun nextDay(): LocalDate = Calendar.getInstance()
            .apply { set(year, month, dayOfMonth); add(Calendar.DATE, 1) }
            .let {
                LocalDate(
                        year = it.get(Calendar.YEAR),
                        month = it.get(Calendar.MONTH),
                        dayOfMonth = it.get(Calendar.DATE)
                )
            }

    override fun toString(): String {
        return "LocalDate(year=$year, month=$month, dayOfMonth=$dayOfMonth)"
    }
}

class DateRange(val start: LocalDate, val end: LocalDate)

/**
 * Zadanie 1.
 * Zaimplementuj porównywanie dat
 *
 * Zadanie 2.
 * Użyj pętli for i zakresów aby przeiterować po datach i wypisać je na ekran.
 *
 * Zadanie 3.
 * Iterując po roku sprawdź czy zakres ma 365 dni
 *
 * Zadanie 4.
 * Obiekty daty powinno dać się porównywać
 */
class Part7 {
    @Test
    fun comparesDates() {
        assertTrue(LocalDate(2017, 0, 1) > LocalDate(2016, 0, 1))
    }
}
