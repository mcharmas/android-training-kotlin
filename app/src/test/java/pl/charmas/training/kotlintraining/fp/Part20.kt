package pl.charmas.training.kotlintraining.fp

abstract class Presenter<out UI>(protected val ui: UI)

class ItemPresenter(ui: ItemUI) : Presenter<ItemPresenter.ItemUI>(ui) {

    fun removeItem(itemId: Long) {
        println("Item $itemId removed!")
        ui.onItemRemoved()
    }

    fun undoRemoval(itemId: Long) {
        println("Removal of item $itemId udone!")
    }

    interface ItemUI {
        fun onItemRemoved()
    }
}


/**
 * Zadanie 1.
 * Zaimplementuj funkcjonalność cofania usunięcia elementu w powyższym kodzie.
 * Użyj do tego funkcji wyższego rzędu w widoku, która przyjmować będzie funkcję umożliwiającą
 * cofanie usunięcia.
 */
class Part20 {
}
