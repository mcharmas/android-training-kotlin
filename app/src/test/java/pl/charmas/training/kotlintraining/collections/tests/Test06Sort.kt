package pl.charmas.training.kotlintraining.collections.tests

import junit.framework.Assert.assertEquals
import org.junit.Test
import pl.charmas.training.kotlintraining.collections.exercises.getCustomersSortedByNumberOfOrders

class Test06Sort {
    @Test
    fun testGetCustomersSortedByNumberOfOrders() {
        assertEquals(sortedCustomers, shop.getCustomersSortedByNumberOfOrders())
    }
}
