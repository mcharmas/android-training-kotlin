package pl.charmas.training.kotlintraining.basicsyntax

import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Skonwertuj kod Javowy do kotlinowego za pomocą IntellijIDEA.
 * Ctrl + Shift + A -> Convert Java File to Kotlin File
 */
class Part4 {
    @Test
    fun worksConverted() {
        val result = Part4JavaCode().toJSON(mutableListOf(1, 2))
        assertEquals(result, "[1, 2]")
    }
}
