package pl.charmas.training.kotlintraining.collections.tests

import junit.framework.Assert.assertEquals
import org.junit.Test
import pl.charmas.training.kotlintraining.collections.exercises.getCustomersWithMoreUndeliveredOrdersThanDelivered

class Test09Partition {
    @Test
    fun testGetCustomersWhoHaveMoreUndeliveredOrdersThanDelivered() {
        assertEquals(setOf(customers[reka]), shop.getCustomersWithMoreUndeliveredOrdersThanDelivered())
    }
}
