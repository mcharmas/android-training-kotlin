package pl.charmas.training.kotlintraining.basicsyntax

import org.junit.Assert.assertEquals
import org.junit.Test

fun joinToString(elements: Collection<String>): String = TODO()

/**
 * Zadanie 1.
 * Zaimplementuj funkcję joinToString tak aby łączyła wszystkie napisy z kolekcji.
 *
 * Zadanie 2.
 * Dodaj opcję separatora prefixu i postfixu.
 * Jeśli to konieczne dopisz testy jednostkowe.
 *
 * Zadanie 3.
 * Uczyń separator prefix i postfix argumentami z domyślnymi wartościami.
 * Jeśli to konieczne dopisz testy jednostkowe.
 *
 * Zadanie 4.
 * Wywołuj funkcję joinToString z testów podając również nazwy parametrów.
 */
class Part6 {

    @Test
    fun first() {
        assertEquals(joinToString(listOf("a", "b", "c")), "abc")
    }
}
