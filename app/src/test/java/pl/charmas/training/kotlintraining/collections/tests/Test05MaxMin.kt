package pl.charmas.training.kotlintraining.collections.tests

import junit.framework.Assert.assertEquals
import org.junit.Test
import pl.charmas.training.kotlintraining.collections.exercises.getCustomerWithMaximumNumberOfOrders
import pl.charmas.training.kotlintraining.collections.exercises.getMostExpensiveOrderedProduct

class Test05MaxMin {
    @Test
    fun testCustomerWithMaximumNumberOfOrders() {
        assertEquals(customers[reka], shop.getCustomerWithMaximumNumberOfOrders())
    }

    @Test
    fun testTheMostExpensiveOrderedProduct() {
        assertEquals(rubyMine, customers[nathan]!!.getMostExpensiveOrderedProduct())
    }
}
