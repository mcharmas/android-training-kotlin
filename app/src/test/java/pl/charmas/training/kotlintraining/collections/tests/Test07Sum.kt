package pl.charmas.training.kotlintraining.collections.tests

import junit.framework.Assert.assertEquals
import org.junit.Test
import pl.charmas.training.kotlintraining.collections.exercises.getTotalOrderPrice

class Test07Sum {
    @Test
    fun testGetTotalOrderPrice() {
        assertEquals(148.0, customers[nathan]!!.getTotalOrderPrice(), 0.001)
    }

    @Test
    fun testTotalPriceForRepeatedProducts() {
        assertEquals(586.0, customers[lucas]!!.getTotalOrderPrice(), 0.001)
    }
}
