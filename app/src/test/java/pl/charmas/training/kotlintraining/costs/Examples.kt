package pl.charmas.training.kotlintraining.costs

/**
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * Premature optimization is the root of all evil.
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */

/** 1. Higher-order functions and Lambda expressions
 * - capturing lambdas vs non capturing
 * - example on transaction
 **/

/** 2. Boxing in lambdas
 * Specific interfaces in Java - generic in Kotlin
 **/

/** 3. Companion Objects
 * Problems:
 * - Getters
 * - Solution: const
 **/
//class MyClass {
//    companion object {
//        private val TAG = "TAG"
//    }
//
//    fun helloWorld() {
//        println(TAG)
//    }
//}

/** 4. Local functions **/
//fun someMath(a: Int): Int {
//    fun sumSquare(b: Int) = (a + b) * (a + b)
//
//    return sumSquare(1) + sumSquare(2)
//}

// Optimised non capturing
//fun someMath(a: Int): Int {
//    fun sumSquare(a: Int, b: Int) = (a + b) * (a + b)
//
//    return sumSquare(a, 1) + sumSquare(a, 2)
//}

/** 5. Null safety runtime checks **/

/** 6. Nullable primitives
 *  - mention primitive arrays!
 **/

/** 7. Varargs with spread operator **/

/** 8. Delegate properties
 * - mention lazy params options (SYNCHRONIZED/NONE etc.)
 **/

/** 9. Show for loops and iterations **/
